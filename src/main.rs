use std::env;
use std::fmt::{Display, Error, Formatter};
use std::fs;
use std::process;

const MAX_DAILY_TRAVEL_TIME: u32 = 6 * 60;
const MAX_NUM_TRAVEL_DAYS: u32 = 5;

#[derive(Debug, Clone)]
struct Hotel {
    dist: u32,
    rating: f32,
}

impl Display for Hotel {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(
            f,
            "Hotel mit einer Bewertung von {:?}, das in {}min vom Startpunkt aus erreichbar ist",
            self.rating,
            self.dist
        )
    }
}

fn main() {
    const USAGE: &str = "Benutzung: a2 <eingabedatei>";

    let mut args = env::args();
    args.next();

    // Speichere den Dateipfad
    let path = args.next().unwrap_or_else(|| {
        // Beende das Programm, wenn kein Dateipfad angegeben wurde
        eprintln!("{}", USAGE);
        process::exit(1);
    });

    // Beende das Programm, wenn zu viele Argumente angegeben wurden
    if args.next().is_some() {
        eprintln!("{}", USAGE);
        process::exit(1);
    }

    // Lese die gegebene Datei ein
    let file = fs::read_to_string(path).expect("Die Datei konnte nicht geöffnet werden");

    // Wandle die Eingabedatei in eine Liste an Hotels um
    let mut lines = file.lines();

    let num_hotels: usize = lines.next().unwrap().parse().unwrap();
    let total_dist: u32 = lines.next().unwrap().parse().unwrap();

    let mut hotels: Vec<Hotel> = Vec::with_capacity(num_hotels + 1);

    for line in lines {
        let mut words = line.split_whitespace();

        let dist: u32 = words.next().unwrap().parse().unwrap();
        let rating: f32 = words.next().unwrap().parse().unwrap();

        hotels.push(Hotel { dist, rating });
    }

    // Hänge das Ziel an die Hotelliste an
    hotels.push(Hotel {
        dist: total_dist,
        rating: f32::MAX,
    });

    // Prüfe, ob überhaupt eine gültige Route existiert
    if !contains_route(&hotels) {
        println!("Es existiert keine den Zeitbeschränkungen entsprechende Route durch die gegebenen Hotels.");
        process::exit(2);
    }

    // Verkleinere die Hotelliste so weit wie möglich
    loop {
        let new_hotels = delete_worst_hotels(&hotels);
        if !contains_route(&new_hotels) {
            break;
        }
        hotels = new_hotels;
    }

    // Finde eine Route und gib sie aus
    let sol = find_route(&hotels);
    print!("Sie müssen zuerst zum {}", sol[0]);
    for hotel in sol.iter().take(sol.len() - 1).skip(1) {
        print!(", dann zum {}", hotel);
    }
    println!(
        ", um dann ihr Ziel zu erreichen, was maximal {}min gebraucht haben dürfte.",
        hotels[hotels.len() - 1].dist
    );
}

fn delete_worst_hotels(hotels: &[Hotel]) -> Vec<Hotel> {
    // Berechne die schlechteste Bewertung
    let mut worst_rating = hotels[0].rating;
    for hotel in hotels.iter().take(hotels.len() - 1).skip(1) {
        if hotel.rating < worst_rating {
            worst_rating = hotel.rating;
        }
    }

    hotels
        .iter()
        .filter(|hotel| hotel.rating > worst_rating)
        .cloned()
        .collect()
}

fn contains_route(hotels: &[Hotel]) -> bool {
    let mut curr = 0;
    let mut days = 1;

    for i in 0..hotels.len() {
        if days > MAX_NUM_TRAVEL_DAYS {
            return false;
        }

        if i == hotels.len() - 1 {
            if hotels[i].dist <= curr + MAX_DAILY_TRAVEL_TIME {
                return true;
            }
            return false;
        }

        if hotels[i].dist <= curr + MAX_DAILY_TRAVEL_TIME
            && hotels[i + 1].dist > curr + MAX_DAILY_TRAVEL_TIME
        {
            curr = hotels[i].dist;
            days += 1;
        }
    }

    false
}

fn find_route(hotels: &[Hotel]) -> Vec<Hotel> {
    let mut curr = 0;

    let mut res: Vec<Hotel> = Vec::new();

    for i in 0..hotels.len() - 1 {
        if hotels[i].dist <= curr + MAX_DAILY_TRAVEL_TIME
            && hotels[i + 1].dist > curr + MAX_DAILY_TRAVEL_TIME
        {
            res.push(hotels[i].clone());
            curr = hotels[i].dist;
        }
    }
    res.push(hotels[hotels.len() - 1].clone());

    // Eine gültige Route muss existieren
    debug_assert!(contains_route(&res));

    res
}
